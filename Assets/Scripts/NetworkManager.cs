using UnityEngine;
using System.Collections;

public class NetworkManager : MonoBehaviour
{
    private const string typeName = "DAQRI_VideoStreaming";
	private const string gameName = "DAQRI_PlayServer";

    private bool isRefreshingHostList = false;
    private HostData[] hostList;

    //public GameObject playerPrefab;

    void OnGUI()
    {
        if (!Network.isClient && !Network.isServer)
        {
            if (GUI.Button(new Rect(100, 100, 250, 100), "Start Server"))
                StartServer();

			//if (GUI.Button(new Rect(100, 250, 250, 100), "Refresh Hosts"))
				//RefreshHostList();
			
			if (hostList != null)
			{
				for (int i = 0; i < hostList.Length; i++)
				{
					if (GUI.Button(new Rect(400, 100 + (110 * i), 300, 100), hostList[i].gameName))
					{
						//Debug.Log("Server: name: " + hostList[i].gameName);
						//Debug.Log("Server: ip: " + hostList[i].ip[0]);
						//Debug.Log("Server: limit: " + hostList[i].playerLimit);
						//Debug.Log("Server: players: " + hostList[i].connectedPlayers);

						//JoinServer(hostList[i]);
					}
				}
			}

        }

    }

    private void StartServer()
    {
		Debug.Log("Server: StartServer");

        Network.InitializeServer(25, 25000, !Network.HavePublicAddress());
		MasterServer.dedicatedServer = true;
		//Network.InitializeServer(5, 25000, !Network.HavePublicAddress());

    }

    void OnServerInitialized()
    {
		Debug.Log("Server: OnServerInitialized");

		MasterServer.RegisterHost(typeName, gameName);
      	//SpawnPlayer();
    }
	
	void OnMasterServerEvent(MasterServerEvent msEvent) 
	{
		if (msEvent == MasterServerEvent.RegistrationSucceeded)
		{
			Debug.Log("Server: registered");
			ReadHostData();
		}
	}

	void OnPlayerConnected(NetworkPlayer player)
	{
		// Disable OnSerializeNetworkView until the client tells us what the view id to use is
		//NetworkView nView = gameObject.GetComponent<NetworkView>();
		//Network.SetSendingEnabled(nView.group, false);
		Debug.Log("**** Player connected from " + player.ipAddress + ":" + player.port);
	}
	
	void OnPlayerDisconnected(NetworkPlayer player)
	{
		Debug.Log("**** Clean up after player " + player);
		Network.RemoveRPCs(player);
		Network.DestroyPlayerObjects(player);
	}

    void Update()
    {
        if (isRefreshingHostList && MasterServer.PollHostList().Length > 0)
        {
			Debug.Log("Server: isRefreshingHostList");

            isRefreshingHostList = false;
            hostList = MasterServer.PollHostList();
        }
    }

    private void RefreshHostList()
    {
        if (!isRefreshingHostList)
        {
            isRefreshingHostList = true;
            MasterServer.RequestHostList(typeName);
        }
    }

	private void ReadHostData()
	{



		Debug.Log("NetworkManager: ip: " + MasterServer.ipAddress + ":" + MasterServer.port);
		Debug.Log("NetworkManager: rate: " + MasterServer.updateRate);
		Debug.Log("NetworkManager: dedicated: " + MasterServer.dedicatedServer);

		/*
		foreach (HostData tmpHostData in hostList) 
		{
			Debug.Log("NetworkManager: name: " + tmpHostData.gameName);
			Debug.Log("NetworkManager: gameType: " + tmpHostData.gameType);
			Debug.Log("NetworkManager: guid: " + tmpHostData.guid);
			Debug.Log("NetworkManager: ip: " + tmpHostData.ip[0] + ":" + tmpHostData.port + ", " + tmpHostData.ip.Length);
			Debug.Log("NetworkManager: playerLimit: " + (string.Empty + hostList[0].playerLimit));
			Debug.Log("NetworkManager: connectedPlayers: " + (string.Empty + hostList[0].connectedPlayers));
			Debug.Log("NetworkManager: nat: " + hostList[0].useNat);
		}
*/

	}
	
    //private void JoinServer(HostData hostData)
    //{
       // Network.Connect(hostData);
    //}

    //void OnConnectedToServer()
    //{
        //SpawnPlayer();
    //}

   // private void SpawnPlayer()
   // {
   //     Network.Instantiate(playerPrefab, Vector3.up * 5, Quaternion.identity, 0);
   // }
}
