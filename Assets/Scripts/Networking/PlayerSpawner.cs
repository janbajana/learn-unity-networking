﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

public class PlayerSpawner : NetworkBehaviour {
	
	[SyncVar]
	public float speed = 10f;

	[SyncVar(hook="ChangeColor")]
	public Vector3 _playerColor = Vector3.one;
	
	//[SyncList]
	//public uint[] _data;
	
	//[SyncListInt]
	public SyncListUInt SyncListIntMy = new SyncListUInt();
	//public List<SyncListUInt> arraySyncListIntMy = new List<SyncListUInt>(64);

	//public uint[] _array;
	
	//public List<uint> _myList;
	
	// Use this for initialization
	void Start () 
	{
		//byte.MaxValue
		ChangeColor (_playerColor);

		/*
		_myList = new List<uint> (300);
		uint size = 800;
		for (uint i = 0; i < size; i++) {
			//[SyncVar]
			_myList.Add((uint) i);
		}
		*/

		//_myList = new List<uint> (300);

		/*
		uint size = 64;
		for (uint i = 0; i < size; i++) 
		{
			//[SyncVar]
			SyncListUInt tmpSyncListUInt = new SyncListUInt();
			arraySyncListIntMy.Add(tmpSyncListUInt);
		}
*/
		//Debug.Log ("PlayerSpawner: Start: " + _myList.Count + ": " + _myList.Capacity + ": " + _myList.ToArray().Length);
		
		//SyncListIntMy.InitializeBehaviour ();
		//SyncListIntMy =  new SyncListInt() {1, 2, 3, 4, 5};
		
		if(isLocalPlayer)
		{
			SyncListIntMy.Callback += OnUIntChanged;
		}

		//ReadList ();
		if (isServer) 
		{
			int size = 921600;
			//size = 50000;
			//size = 14000;
			size = 10;
			for (int i = 0; i < size; i++) {
				//[SyncVar]
				SyncListIntMy.Add((uint) i);
			}
			//ReadList ();
		}
	}
	
	private void MyCallback(SyncListInt.Operation op, int index)
	{
		//Debug.Log("MyCallback Called: Op=" + op.ToString() + " Index=" + index + " New Array Length=" + SyncListIntMy.Count);
		//for(int i = 0; i < SyncListIntMy.Count; i++)
		//{
		//	Debug.Log("syncListInt[" + i + "]=" + SyncListIntMy[i]);
		//}
	}
	
	private void OnUIntChanged(SyncListInt.Operation op, int index)
	{
		Debug.Log("Client list: " + op + ",ind: " + index + ", co: " + SyncListIntMy.Count);
		
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
		if(isServer)
		{

			OnServerInputs();
		}
		
		if(isLocalPlayer)
		{
			if (Input.GetKey(KeyCode.Z))
			{
				CmdSendDataFromClient(10);
			}		
		}

		if(isLocalPlayer)
		{
			InputMovement();
			InputColorChange();
		}
		
	}

	private void OnServerInputs()
	{

		if (Input.GetKeyDown(KeyCode.R))
		{
			RpcRespawnObjects();
		}

		if (Input.GetKey(KeyCode.Q))
		{
			//_testValue ++;
			
			SyncListIntMy[5] ++;
			//SyncListIntMy[50] ++;
			//_dataChanaged = true;
		}
		
		if (Input.GetKey(KeyCode.E))
		{
			//_testValue --;
			//SyncListIntMy[50] --;
			SyncListIntMy[5] --;
			//_dataChanaged = true;
		}
	}

	private void InputMovement()
	{
		if (Input.GetKey(KeyCode.W))
		{
			transform.position = (transform.position + Vector3.forward * speed * Time.deltaTime);
			//Debug.Log ("pos: " + transform.position.x);
		}
		
		if (Input.GetKey(KeyCode.S))
		{
			transform.position = (transform.position - Vector3.forward * speed * Time.deltaTime);
			//Debug.Log ("pos: " + transform.position.x);
		}
		
		if (Input.GetKey(KeyCode.D))
		{
			transform.position = (transform.position + Vector3.right * speed * Time.deltaTime);
			//Debug.Log ("pos: " + transform.position.x);
		}
		
		if (Input.GetKey(KeyCode.A))
		{
			transform.position = (transform.position - Vector3.right * speed * Time.deltaTime);
			//Debug.Log ("pos: " + transform.position.x);
		}
	}

	private void InputColorChange()
	{
		if (Input.GetKeyDown(KeyCode.C))
			CmdChangeColorTo(new Vector3(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f)));
	}
	
	void ReadList()
	{
		
		//Debug.Log ("Clisnt: ReadList: ");
		//Debug.Log (SyncListIntMy);
		
		//Debug.Log ("0: " + SyncListIntMy[0]);

		for (int i = 0; i < SyncListIntMy.Count; i++) 
		{
			//Debug.Log (i + ": " + SyncListIntMy[i]);
			//syncListInt.Add(i);
		}
	}

	//hook functions
	void ChangeColor(Vector3 color)
	{
		Renderer tmpRenderer = GetComponentInChildren<Renderer> ();
		tmpRenderer.material.color = new Color(color.x, color.y, color.z, 1f);
	}

	//commands
	[Command]
	void CmdSendDataFromClient(int value)
	{
		Debug.Log ("Server: CmdSendDataFromClient: " + value);
	}

	[Command]
	void CmdChangeColorTo(Vector3 color)
	{
		_playerColor = color;
		Renderer tmpRenderer = GetComponentInChildren<Renderer> ();
		tmpRenderer.material.color = new Color(color.x, color.y, color.z, 1f);

		//NetworkServer.SpawnObjects ();
		//NetworkServer.Spawn (tmoRenderer.gameObject);
		//if (GetComponent<NetworkView>().isMine)
			//GetComponent<NetworkView>().RPC("ChangeColorTo", RPCMode.OthersBuffered, color);
	}

	//rpcs
	[ClientRpc]
	void RpcRespawnObjects()
	{
		transform.position = Vector3.zero;
	}

	[ClientRpc]
	void RpcSendDataFromServer(uint[] amount)
	{
		if(amount.Length > 0)
		{
			Debug.Log("Client: RpcSendData: " + amount.Length);
		}
		
	}
	
}