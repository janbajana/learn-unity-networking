﻿using UnityEngine;
using System.Collections;

public class NetManager : MonoBehaviour {
	
	[SerializeField]
	private bool _isServer = false;
	private bool _isClient = false;
	
	private int number = 0;
	//private UDPClient _client;
	//private UDPServer _server;
	
	void Awake()
	{
		if (_isServer) 
		{
			Debug.LogWarning("NetManager: Initialize UDP server");
			_isClient = false;
			//_listener = new UDPListener ("224.5.6.7", "5000", "1", "2");
			//_server = new UDPServer ("224.168.100.2", "11000", "1");
			UDPServer.StartMulticastGroup("224.168.100.2", "11000", "1");
			
			//"224.168.100.2"
		}
		else
		{
			Debug.LogWarning("NetManager: Initialize UDP client");
			_isClient = true;
			//_client = new UDPClient ("224.5.6.7","5000");
			UDPClient.StartMulticastGoup("224.168.100.2","11000");
			UDPClient.GetMulticastOptionProperties();
		}
	}
	
	// Use this for initialization
	void Start () 
	{
		
		
		
	}
	
	void OnApplicationQuit()
	{
		Debug.LogWarning("NetManager: OnApplicationQuit");
		
		if (_isServer) 
		{
			UDPServer.CloseSocket ();
		}else
		{
			UDPClient.CloseSocket ();
		}
		
	}
	
	// Update is called once per frame
	void Update () 
	{


		if(_isServer)
		{
			
			if (Input.GetKey(KeyCode.Z))
			{
				Debug.Log("NetManager: Sending message");
				UDPServer.MulticastMessage("This is my sent message: " + (number++));
			}
			
		}
		
		if(_isClient && UDPClient._messageReceived)
		{
			
			
			//Debug.Log("NetManager: Recieve: " + UDPClient._countPacket + ", size: " + UDPClient._sizePacket + ",rS: " + UDPClient._sizeMessage);
			
			if (Input.GetKey(KeyCode.Z))
			{
				UDPClient._countPacket = 0;
			}
			
			//UDPClient._messageReceived = false;
			//if (_client._messageReceived) 
			//{
			//Debug.Log("NetManager: Sending message: " + _client._message);
			//}
		}
		
	}
	
}



