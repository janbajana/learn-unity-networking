﻿using UnityEngine;
using System.Collections;

public class ClientManager : MonoBehaviour {

	private const string typeName = "DAQRI_VideoStreaming";
	//private const string gameName = "DAQRI_PlayServer";
	
	private bool isRefreshingHostList = false;
	private HostData[] hostList;
	
	public GameObject playerPrefab;

	void OnConnectedToServer()
	{
		SpawnPlayer();
		Debug.Log("Client: OnConnectedToServer");
	}

	private void JoinServer(HostData hostData)
	{

		HostData myHostData = new HostData();
		myHostData.gameName = "DAQRI_PlayServer";
		myHostData.gameType = typeName;
		myHostData.port = 25000;
		//myHostData.ip = new string[1] {"192.168.128.152"};
		myHostData.ip = new string[1] {"127.0.0.1"};
		myHostData.useNat = false;

		Debug.Log("name: " + hostList[0].gameName);
		Debug.Log("gameType: " + hostList[0].gameType);
		Debug.Log("guid: " + hostList[0].guid);
		Debug.Log("port: " + hostList[0].port);
		Debug.Log("ip: " + hostList[0].ip[0] + hostList[0].ip.Length);
		Debug.Log("playerLimit: " + (string.Empty + hostList[0].playerLimit));
		Debug.Log("connectedPlayers: " + (string.Empty + hostList[0].connectedPlayers));
		Debug.Log("nat: " + hostList[0].useNat);

		Network.Connect(hostData);
		//Network.Connect("127.0.0.1", 25000);
	}

	void OnGUI()
	{
		if (!Network.isClient)
		{
			
			if (GUI.Button(new Rect(100, 250, 250, 100), "Refresh Hosts"))
				RefreshHostList();
			
			if (hostList != null)
			{
				for (int i = 0; i < hostList.Length; i++)
				{
					if (GUI.Button(new Rect(400, 100 + (110 * i), 300, 100), hostList[i].gameName))
					{

						JoinServer(hostList[i]);

					}

				}

				if(hostList[0] != null)
				{
				GUI.Button(new Rect(400, 200 + (51 * 0), 300, 50), hostList[0].gameName);
				GUI.Button(new Rect(400, 200 + (51 * 1), 300, 50), hostList[0].gameType);
				GUI.Button(new Rect(400, 200 + (51 * 2), 300, 50), (string.Empty + hostList[0].port));
				GUI.Button(new Rect(400, 200 + (51 * 3), 300, 50), hostList[0].ip[0]);
				GUI.Button(new Rect(400, 200 + (51 * 4), 300, 50), (string.Empty + hostList[0].playerLimit));
				GUI.Button(new Rect(400, 200 + (51 * 5), 300, 50), (string.Empty + hostList[0].connectedPlayers));
				}

			}
			
		}
		
	}

	void OnMasterServerEvent(MasterServerEvent msEvent) 
	{
		if (msEvent == MasterServerEvent.RegistrationSucceeded)
			Debug.Log("Client: registered");
		
	}
	
	void Update()
	{
		if (isRefreshingHostList && MasterServer.PollHostList().Length > 0)
		{
			Debug.Log("Server: isRefreshingHostList");
			
			isRefreshingHostList = false;
			hostList = MasterServer.PollHostList();
		}
	}
	
	private void RefreshHostList()
	{
		if (!isRefreshingHostList)
		{
			isRefreshingHostList = true;
			MasterServer.RequestHostList(typeName);
		}
	}

	private void SpawnPlayer()
	{
		Network.Instantiate(playerPrefab, Vector3.up, Quaternion.identity, 0);
	}
}
