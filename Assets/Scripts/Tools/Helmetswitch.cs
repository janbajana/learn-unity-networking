﻿using UnityEngine;
using System.Collections;

public class Helmetswitch : MonoBehaviour 
{

	private bool haveSetupForHelmet = false;
	public bool helmetVersion;
	GameObject _videoObj;
	// Use this for initialization
	void Start () 
	{
		haveSetupForHelmet = !helmetVersion;
	}
	
	// Update is called once per frame
	void Update () 
	{

		if(Input.GetKeyDown(KeyCode.V))
		{
			helmetVersion = !helmetVersion;
		}

		if (helmetVersion != haveSetupForHelmet)
		{
				if(_videoObj == null)
					_videoObj = GameObject.Find("Video background");

				if(_videoObj)
				{
					_videoObj.SetActive(!helmetVersion);
					haveSetupForHelmet = helmetVersion;
				}

			GameObject videoObjL = GameObject.Find("Video background (L)");
			GameObject videoObjR = GameObject.Find("Video background (R)");

			if(videoObjL && videoObjR)
			{
				Debug.LogWarning("lalala");
				videoObjL.SetActive(!helmetVersion);
				videoObjR.SetActive(!helmetVersion);
			}

			//if(videoObjR)
			//{
			//}


		}
	}
}
