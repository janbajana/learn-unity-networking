﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

using System;
using System.Text;


public class TextureRenderer : MonoBehaviour {
	
	[SerializeField]
	Image imagePreview;  // drag the render texture onto this field in the Inspector
	
	//[SerializeField]
	public ARController _globalARController;  // drag the render texture onto this field in the Inspector
	
	//[SerializeField]
	public RenderTexture defaultCameraTexture;  // drag the render texture onto this field in the Inspector
	
	//[SerializeField]
	public RenderTexture securityCameraTexture;  // drag the render texture onto this field in the Inspector
	
	//[SerializeField]
	public Camera _AROriginSecurityCamera; // drag the security camera onto this field in the inspector
	
	//[SerializeField]
	public Camera _GUISecurityCamera; // drag the security camera onto this field in the inspector
	
	[SerializeField]
	Camera _BackgroundSecurityCamera; // drag the security camera onto this field in the inspector
	
	public bool _readPixels = true;
	
	//private int _BUFFER_SIZE = 9210;
	private int _BUFFER_SIZE = 1024;
	//private int _BUFFER_SIZE = 20;
	
	int _countPacket = 0;
	int _countFrames = 0;
	
	//public const int BUFFER_SIZE = (65507);
	private byte[] _mybuffer;// = new byte[_BUFFER_SIZE];
	
	Sprite _cameraSprite;
	Texture2D _cameraImage;
	bool _makeScree = false;
	int _imageCounter = 0;
	private float _timeLasUpdate;
	private float _streemFPS = 30f;
	
	private Vector3 syncEndPosition = Vector3.zero;
	
	// Use this for initialization
	void Start () 
	{

		PeerConnectionJavaManager.make ();

		_mybuffer = new byte[_BUFFER_SIZE];
		_countPacket = 0;
		_countFrames = 0;
		_timeLasUpdate = Time.time;
		//_cameraImage = new Texture2D(_AROriginSecurityCamera.targetTexture.width, _AROriginSecurityCamera.targetTexture.height, TextureFormat.RGB24, false);
		//_cameraImage = new Texture2D(640, 480, TextureFormat.RGB24, false);
		_cameraImage = new Texture2D(320, 240, TextureFormat.RGB24, false);
		
		
		_streemFPS = 30f;
		//_BackgroundSecurityCamera = _globalARController.GetLeftCamera ();
		//_cameraSprite = Sprite.Create(_cameraImage, new Rect(0, 0, 640f, 480f), new Vector2(0.5f,0.5f));
		//imagePreview.sprite = _cameraSprite;
		//imagePreview.sprite.
	}
	
	// Update is called once per frame
	void Update () 
	{
		
		//Debug.Log ("Update call");
		if (Input.GetKeyDown ("p")) 
		{
			_countPacket = 0;
			//_makeScree = true;
		}
		
		if (Input.GetKey (KeyCode.O)) {
			_makeScree = true;
		} else {
			_makeScree = false;
		}
		
		if (_BackgroundSecurityCamera == null) {
			_BackgroundSecurityCamera = _globalARController.GetBackgroundCamera0 ();
		}
		
		//Debug.Log("FrameUpdate: " + Time.time + ", " + _timeLasUpdate);
		
		if ((Time.time - _timeLasUpdate)*_streemFPS > 1.0)
		{
			StartCoroutine (RenderCameraView ());
			//Debug.Log("FrameUpdate: " + Time.time);
			_timeLasUpdate = Time.time;
		}
		
		
	}
	
	public IEnumerator RenderCameraView()
	{
		yield return new WaitForEndOfFrame();
		
		RenderTexture oldRendText = RenderTexture.active;
		RenderTexture.active = securityCameraTexture;
		
		if (_BackgroundSecurityCamera != null) 
		{
			_BackgroundSecurityCamera.targetTexture = securityCameraTexture; 
			_BackgroundSecurityCamera.Render (); 
		}

		if (_AROriginSecurityCamera != null) {
			_AROriginSecurityCamera.targetTexture = securityCameraTexture; 
			_AROriginSecurityCamera.Render (); 
		}

		if (_GUISecurityCamera != null) {
			_GUISecurityCamera.targetTexture = securityCameraTexture; 
			_GUISecurityCamera.Render (); 
		}

		if (_readPixels) 
		{
			//_cameraImage = new Texture2D(_AROriginSecurityCamera.targetTexture.width, _AROriginSecurityCamera.targetTexture.height, TextureFormat.RGB24, false);
			_cameraImage.ReadPixels(new Rect(0, 0, _AROriginSecurityCamera.targetTexture.width, _AROriginSecurityCamera.targetTexture.height), 0, 0);
			_cameraImage.Apply();
		}
		
		//imagePreview.sprite.texture.ReadPixels (new Rect(0, 0, _AROriginSecurityCamera.targetTexture.width, _AROriginSecurityCamera.targetTexture.height), 0, 0);
		//imagePreview.sprite.texture.Apply();

		if (_AROriginSecurityCamera != null) 
			_AROriginSecurityCamera.targetTexture = null;

		if (_GUISecurityCamera != null) 
			_GUISecurityCamera.targetTexture = null;
		
		if (_BackgroundSecurityCamera != null) 
			_BackgroundSecurityCamera.targetTexture = null; 

			//_BackgroundSecurityCamera.targetTexture = defaultCameraTexture; 
		
		RenderTexture.active = oldRendText;

		ProcessData (ref _cameraImage);

		//bool enabled = false;
		if (_makeScree) 
		{
			_makeScree = false;
			byte[] pBytes = _cameraImage.EncodeToPNG ();
			Debug.Log ("Data: " + pBytes.Length + ",: " + _cameraImage.GetPixels().Length);
			Debug.Log ("Size: " + _cameraImage.width + ",: " + _cameraImage.height);
			
			
			// save the encoded image to a file
			System.IO.File.WriteAllBytes (Application.persistentDataPath + "/camera_image" + _imageCounter + ".png", pBytes);
			Debug.Log (Application.persistentDataPath + "/camera_image.png");
			_imageCounter++;
		}
		
	}

	private void ProcessData(ref Texture2D image)
	{
		//byte[] pBytes = image.GetRawTextureData ();
		//image.GetPixels().

		PeerConnectionJavaManager.PrintTestData ();

	}

	private void ProcessDataOld()
	{
		//this is testing staff for read pixels and networking.
		if (_makeScree == true) 
		{
			
			//testing
			byte[] bytes = _cameraImage.EncodeToPNG ();
			string _sizeMess = string.Empty;
			
			int packetCount = bytes.Length / _mybuffer.Length;
			
			for(int i = 0; i<=packetCount; i++)
			{
				//_mybuffer[0] = (byte) i;
				_countPacket++;
				
				byte[] sendMessage = ASCIIEncoding.ASCII.GetBytes(string.Empty + _countPacket);
				
				sendMessage.CopyTo(_mybuffer, 0);
				_sizeMess = Encoding.ASCII.GetString(_mybuffer, 0, sendMessage.Length);
				
				//System.Convert.To
				//byte* ps = bytes + i*(_mybuffer.Length-1);
				
				int offset = i*_mybuffer.Length;
				int size = 0;
				
				if((bytes.Length-offset) < _mybuffer.Length)
				{
					
					size = bytes.Length-offset;
				}else
				{
					
					size = _mybuffer.Length;
					
				}
				
				//Debug.Log ("size: " + size + ", " + offset + ", " + (bytes.Length-offset));
				
				UDPServer.MulticastData(bytes, offset, size);
				
				//UDPServer.MulticastData (_mybuffer);
			}
			_countFrames++;
			Debug.Log ("number: " + _countPacket + ", frames: " + _countFrames + ", cPacks: " + packetCount + ", iSize: " + bytes.Length);
			//Debug.Log ("ssize: " + _sizeMess + ", " + _sizeMess.Length + ", " + _countPacket);
			//Debug.Log ("ssize: " + _sizeMess);
			
			//UDPServer.MulticastMessage("Sending texturedata1: " + bytes.Length);
			//string mubufff = "Sending data";
			//UDPServer.MulticastMessage (mubufff);

		}

	}

	
}
